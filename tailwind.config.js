module.exports = {
  purge: ["./src/**/*.vue"],
  theme: {
    extend: {
      fontFamily: {
        montserrat: ["Montserrat", "sans-serif"],
        "flood-std": ["Flood Std", "sans-serif"],
        "permanent-marker": ["Permanent Marker", "sans-serif"],
        poppins: ["Poppins", "sans-serif"],
        nunito: ["Nunito", "sans-serif"],
      },
      colors: {
        purple: "#3f3cbb",
        pink: {
          300: "var(--color-pink)",
        },
        red: {
          600: "var(--color-red)",
        },
        yellow: {
          600: "var(--color-yellow)",
        },
      },
    },
  },
  variants: {
    extend: {
      brightness: ["hover", "focus"],
    },
  },
  plugins: [],
};
