import { createApp } from "vue";

import App from "@/App.vue";
import Maska from "maska";
import Toast, { PluginOptions as ToastPluginOptions } from "vue-toastification";
import SwiperCore, { Autoplay, Navigation, Pagination } from "swiper";

import "@/assets/main.css";
import "vue-toastification/dist/index.css";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "@/assets/swiper.css";

import router from "@/router";

const app = createApp(App);

const toastPluginOptions: ToastPluginOptions = {
  timeout: 2000,
};

app.use(router);
app.use(Maska);
app.use(Toast, toastPluginOptions);
SwiperCore.use([Autoplay, Navigation, Pagination]);

app.mount("#app");
