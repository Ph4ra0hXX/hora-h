import { createRouter, createWebHistory, RouterOptions } from "vue-router";

import Home from "@/views/Home/Index.vue";
import Search from "@/views/Search/Index.vue";
import Suite from "@/views/Suite/Index.vue";
import Finally from "@/views/Finally/Index.vue";
import Increase from "@/views/Increase/Index.vue";
import Resume from "@/views/Resume/Index.vue";
import Payment from "@/views/Payment/Index.vue";
import Register from "@/views/Register/Index.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/procurar",
    name: "Search",
    component: Search,
  },
  {
    path: "/suite",
    name: "Suite",
    component: Suite,
  },
  {
    path: "/concluido",
    name: "Finally",
    component: Finally,
  },
  {
    path: "/acrescimo",
    name: "Increase",
    component: Increase,
  },
  {
    path: "/resumo",
    name: "Resume",
    component: Resume,
  },
  {
    path: "/pagamento",
    name: "Payment",
    component: Payment,
  },
  {
    path: "/cadastro",
    name: "Register",
    component: Register,
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/",
  },
];

const router = createRouter(<RouterOptions>{
  history: createWebHistory(),
  routes,
});

export default router;
