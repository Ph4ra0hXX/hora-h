import { Swiper } from "swiper";

export const getIntegerAndFloat = (number: number) => {
  const [integer, float] = number.toFixed(2).toString().split(".");
  return { integer, float };
};

export const maskCardNumber = (number: string) => {
  number = number.replaceAll(" ", "");
  return [
    "*".repeat(number.length > 4 ? 4 : number.length),
    number.length > 4
      ? "*".repeat(number.length - 4 > 4 ? 4 : number.length - 4)
      : "",
    number.length > 8
      ? "*".repeat(number.length - 8 > 4 ? 4 : number.length - 8)
      : "",
    number.length > 12
      ? number.slice(-1 * (number.length - 12 < 5 ? number.length - 12 : 4))
      : "",
  ];
};

export const swiperMouseEvent = {
  enter: (event: Event) => {
    (event.target as any)?.swiper.autoplay.stop();
  },
  leave: (event: Event) => {
    (event.target as any)?.swiper.autoplay.start();
  },
};
