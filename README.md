## Execute

### Local

#### Project setup

```
yarn
```

#### Compiles and hot-reloads for development
```
yarn dev
```

#### Compiles and minifies for production
```
yarn build
```

### Docker

```
docker-compose up -d
docker-compose exec web yarn
docker-compose exec web yarn dev --host
```

## Commit Conventions

This repository follows [angular commit conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md).

Must be one of the following:

- **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests
