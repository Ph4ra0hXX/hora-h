FROM node:lts

WORKDIR /app

RUN npm install -g pm2

EXPOSE 3000